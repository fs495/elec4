#----------------------------------------------------------------------

clean:
	find . \(  -name '*.bck' \
		-o -name '*.kicad_pcb-bak' \
		-o -name '*-cache.lib' \
		-o -name '*.bak' \) -print -exec rm {} \;

layer-clean:
	find . -name '*.svg' -print -exec rm {} \;

3d-clean:
	rm -rf *.wrl shapes3D

pcb-clean:
	find . \(  -name \*.g[tb]a \
		-o -name \*.g[tb]l \
		-o -name \*.g[tb]s \
		-o -name \*.g[tb]p \
		-o -name \*.g[tb]o \
		-o -name \*.gbr \
		-o -name \*.txt \
		-o -name \*.drl \
		-o -name \*.pho \) -print -exec rm {} \;

distclean:
	-$(MAKE) clean layer-clean 3d-clean pcb-clean

#----------------------------------------------------------------------

layer:
	-rm -f $(PCBNAME)_layer.pdf
	convert -density 150 *.svg $(PCBNAME)_layer.pdf

3d:
	-rm -f $(PCBNAME)_3d.zip
	zip -r $(PCBNAME)_3d.zip $(PCBNAME).wrl shapes3D/

pcb:
	rm -rf work
	mkdir work
	cp -p $(PCBNAME).drl work/$(PCBNAME).TXT
	cp -p $(PCBNAME)-F.Cu.gbr work/$(PCBNAME).GTL
	cp -p $(PCBNAME)-B.Cu.gbr work/$(PCBNAME).GBL
	cp -p $(PCBNAME)-F.Mask.gbr work/$(PCBNAME).GTS
	cp -p $(PCBNAME)-B.Mask.gbr work/$(PCBNAME).GBS
	cp -p $(PCBNAME)-F.SilkS.gbr work/$(PCBNAME).GTO
	cp -p $(PCBNAME)-B.SilkS.gbr work/$(PCBNAME).GBO
	cp -p $(PCBNAME)-Edge.Cuts.gbr work/$(PCBNAME).GML
	-rm -f $(PCBNAME)_pcb.zip
	(cd work && zip ../$(PCBNAME)_pcb.zip *)
	rm -r work
