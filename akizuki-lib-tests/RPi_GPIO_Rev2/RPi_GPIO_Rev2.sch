EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RPi_GPIO_Rev2 P1
U 1 1 56889877
P 1550 1100
F 0 "P1" H 1400 1350 60  0000 C CNN
F 1 "RPi_GPIO_Rev2" H 1400 1250 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x13" H 1450 1200 60  0001 C CNN
F 3 "" H 1550 1300 60  0000 C CNN
	1    1550 1100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
