EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MCP3002-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCP3002 U1
U 1 1 568CE011
P 1350 1000
F 0 "U1" H 1100 1050 60  0000 C CNN
F 1 "MCP3002" H 1700 1050 60  0000 C CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 1250 850 60  0001 C CNN
F 3 "" H 1250 850 60  0000 C CNN
	1    1350 1000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
