EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L INRC09SS49T DISP1
U 1 1 571B155C
P 8600 1700
F 0 "DISP1" H 8950 1750 60  0000 C CNN
F 1 "INRC09SS49T" H 8950 -1100 60  0000 C CNN
F 2 "" H 8850 1650 60  0000 C CNN
F 3 "" H 8950 1750 60  0000 C CNN
	1    8600 1700
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR7
U 1 1 571B159E
P 7700 1700
F 0 "#PWR7" H 7700 1550 50  0001 C CNN
F 1 "+5V" H 7700 1840 50  0000 C CNN
F 2 "" H 7700 1700 50  0000 C CNN
F 3 "" H 7700 1700 50  0000 C CNN
	1    7700 1700
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 571B15B6
P 7950 1800
F 0 "R4" V 8030 1800 50  0000 C CNN
F 1 "61" V 7950 1800 50  0000 C CNN
F 2 "" V 7880 1800 50  0000 C CNN
F 3 "" H 7950 1800 50  0000 C CNN
	1    7950 1800
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR8
U 1 1 571B1644
P 7700 2200
F 0 "#PWR8" H 7700 1950 50  0001 C CNN
F 1 "GND" H 7700 2050 50  0000 C CNN
F 2 "" H 7700 2200 50  0000 C CNN
F 3 "" H 7700 2200 50  0000 C CNN
	1    7700 2200
	1    0    0    -1  
$EndComp
$Comp
L LM555N U1
U 1 1 571B16C0
P 4200 3100
F 0 "U1" H 3800 3450 50  0000 L CNN
F 1 "LM555N" H 3800 2750 50  0000 L CNN
F 2 "" H 4200 3100 50  0000 C CNN
F 3 "" H 4200 3100 50  0000 C CNN
	1    4200 3100
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 571B171B
P 3350 2900
F 0 "C1" H 3375 3000 50  0000 L CNN
F 1 "0.001u" H 3375 2800 50  0000 L CNN
F 2 "" H 3388 2750 50  0000 C CNN
F 3 "" H 3350 2900 50  0000 C CNN
	1    3350 2900
	0    -1   -1   0   
$EndComp
$Comp
L C C2
U 1 1 571B177E
P 3350 3100
F 0 "C2" H 3375 3200 50  0000 L CNN
F 1 "0.022u" H 3050 3000 50  0000 L CNN
F 2 "" H 3388 2950 50  0000 C CNN
F 3 "" H 3350 3100 50  0000 C CNN
	1    3350 3100
	0    -1   -1   0   
$EndComp
$Comp
L R R2
U 1 1 571B17A7
P 5000 3300
F 0 "R2" V 5080 3300 50  0000 C CNN
F 1 "6.7k" V 5000 3300 50  0000 C CNN
F 2 "" V 4930 3300 50  0000 C CNN
F 3 "" H 5000 3300 50  0000 C CNN
	1    5000 3300
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 571B1804
P 5000 3100
F 0 "R1" V 5080 3100 50  0000 C CNN
F 1 "6.2k" V 5000 3100 50  0000 C CNN
F 2 "" V 4930 3100 50  0000 C CNN
F 3 "" H 5000 3100 50  0000 C CNN
	1    5000 3100
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR3
U 1 1 571B183F
P 5200 3100
F 0 "#PWR3" H 5200 2950 50  0001 C CNN
F 1 "+5V" H 5200 3240 50  0000 C CNN
F 2 "" H 5200 3100 50  0000 C CNN
F 3 "" H 5200 3100 50  0000 C CNN
	1    5200 3100
	0    1    1    0   
$EndComp
$Comp
L GND #PWR1
U 1 1 571B1984
P 2950 3250
F 0 "#PWR1" H 2950 3000 50  0001 C CNN
F 1 "GND" H 2950 3100 50  0000 C CNN
F 2 "" H 2950 3250 50  0000 C CNN
F 3 "" H 2950 3250 50  0000 C CNN
	1    2950 3250
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR2
U 1 1 571B1C6D
P 3600 3400
F 0 "#PWR2" H 3600 3250 50  0001 C CNN
F 1 "+5V" H 3600 3540 50  0000 C CNN
F 2 "" H 3600 3400 50  0000 C CNN
F 3 "" H 3600 3400 50  0000 C CNN
	1    3600 3400
	-1   0    0    1   
$EndComp
$Comp
L R R3
U 1 1 571B220B
P 5700 2900
F 0 "R3" V 5780 2900 50  0000 C CNN
F 1 "143k" V 5700 2900 50  0000 C CNN
F 2 "" V 5630 2900 50  0000 C CNN
F 3 "" H 5700 2900 50  0000 C CNN
	1    5700 2900
	0    -1   -1   0   
$EndComp
$Comp
L 2SC1815 Q1
U 1 1 571B2268
P 6150 2900
F 0 "Q1" H 6350 2975 50  0000 L CNN
F 1 "2SC1815" H 6350 2900 50  0000 L CNN
F 2 "" H 6350 2825 50  0000 L CIN
F 3 "" H 6150 2900 50  0000 L CNN
	1    6150 2900
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR L1
U 1 1 571B229F
P 6250 2200
F 0 "L1" V 6200 2200 50  0000 C CNN
F 1 "0.47uH" V 6350 2200 50  0000 C CNN
F 2 "" H 6250 2200 50  0000 C CNN
F 3 "" H 6250 2200 50  0000 C CNN
	1    6250 2200
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR4
U 1 1 571B23D2
P 6250 1800
F 0 "#PWR4" H 6250 1650 50  0001 C CNN
F 1 "+5V" H 6250 1940 50  0000 C CNN
F 2 "" H 6250 1800 50  0000 C CNN
F 3 "" H 6250 1800 50  0000 C CNN
	1    6250 1800
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 571B2436
P 7100 2950
F 0 "C3" H 7125 3050 50  0000 L CNN
F 1 "0.1u" H 7125 2850 50  0000 L CNN
F 2 "" H 7138 2800 50  0000 C CNN
F 3 "" H 7100 2950 50  0000 C CNN
	1    7100 2950
	1    0    0    -1  
$EndComp
$Comp
L D D1
U 1 1 571B24AF
P 6700 2600
F 0 "D1" H 6700 2700 50  0000 C CNN
F 1 "SD103A" H 6700 2500 50  0000 C CNN
F 2 "" H 6700 2600 50  0000 C CNN
F 3 "" H 6700 2600 50  0000 C CNN
	1    6700 2600
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR5
U 1 1 571B2761
P 6250 3250
F 0 "#PWR5" H 6250 3000 50  0001 C CNN
F 1 "GND" H 6250 3100 50  0000 C CNN
F 2 "" H 6250 3250 50  0000 C CNN
F 3 "" H 6250 3250 50  0000 C CNN
	1    6250 3250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR6
U 1 1 571B2BFA
P 7100 3250
F 0 "#PWR6" H 7100 3000 50  0001 C CNN
F 1 "GND" H 7100 3100 50  0000 C CNN
F 2 "" H 7100 3250 50  0000 C CNN
F 3 "" H 7100 3250 50  0000 C CNN
	1    7100 3250
	1    0    0    -1  
$EndComp
Text Notes 4850 2450 0    60   ~ 0
typ.\nfreq 56kHz\nduty 67%\n5Vp-p
Text Notes 7000 2250 0    60   ~ 0
typ. 10V
Text Notes 7250 4050 0    60   ~ 0
SEG1
Text Notes 7650 4350 0    60   ~ 0
SEG2
Text Notes 7600 4900 0    60   ~ 0
SEG3
Text Notes 7200 5150 0    60   ~ 0
SEG4
Text Notes 7200 5450 0    60   ~ 0
SEG5
Text Notes 6750 4950 0    60   ~ 0
SEG6
Text Notes 7750 5350 0    60   ~ 0
SEG7
Text Notes 7750 5200 0    60   ~ 0
SEG8
Text Notes 7250 4600 0    60   ~ 0
SEG9
Text Notes 6750 4350 0    60   ~ 0
SEG10
Wire Wire Line
	8400 1800 8100 1800
Wire Wire Line
	8400 1900 8400 1800
Wire Wire Line
	7800 1800 7700 1800
Wire Wire Line
	7700 1800 7700 1700
Wire Wire Line
	8400 2200 8400 2100
Wire Wire Line
	8400 2100 7700 2100
Wire Wire Line
	7700 2100 7700 2200
Wire Wire Line
	5150 3100 5200 3100
Wire Wire Line
	4700 3100 4850 3100
Wire Wire Line
	4750 3100 4750 3200
Wire Wire Line
	4750 3200 5200 3200
Wire Wire Line
	5200 3200 5200 3300
Wire Wire Line
	5200 3300 5150 3300
Connection ~ 4750 3100
Wire Wire Line
	4700 3300 4850 3300
Wire Wire Line
	3500 3100 3700 3100
Wire Wire Line
	3500 2900 3700 2900
Wire Wire Line
	3200 3100 2950 3100
Wire Wire Line
	2950 2900 2950 3250
Wire Wire Line
	3200 2900 2950 2900
Connection ~ 2950 3100
Wire Wire Line
	3700 3300 3600 3300
Wire Wire Line
	3600 3300 3600 3400
Wire Wire Line
	5550 2900 4700 2900
Wire Wire Line
	5850 2900 5950 2900
Wire Wire Line
	6250 2500 6250 2700
Wire Wire Line
	6250 1900 6250 1800
Wire Wire Line
	6550 2600 6250 2600
Connection ~ 6250 2600
Wire Wire Line
	6250 3100 6250 3250
Wire Wire Line
	7100 3250 7100 3100
Wire Wire Line
	8300 2800 8400 2800
Wire Wire Line
	8300 2600 8300 4300
Connection ~ 8300 2600
Wire Wire Line
	8300 2900 8400 2900
Connection ~ 8300 2800
Wire Wire Line
	8400 2700 8300 2700
Connection ~ 8300 2700
Wire Wire Line
	7100 2800 7100 2600
Connection ~ 7100 2600
Wire Notes Line
	5000 2900 5000 2450
Wire Notes Line
	7100 2300 7100 2600
Wire Wire Line
	6850 2600 8400 2600
Wire Notes Line
	7100 4050 7650 4050
Wire Notes Line
	7650 4050 7600 4100
Wire Notes Line
	7600 4100 7150 4100
Wire Notes Line
	7150 4100 7100 4050
Wire Notes Line
	7650 4050 7600 4650
Wire Notes Line
	7600 4650 7550 4600
Wire Notes Line
	7550 4600 7600 4100
Wire Notes Line
	7150 4100 7100 4600
Wire Notes Line
	7100 4600 7050 4650
Wire Notes Line
	7050 4650 7100 4050
Wire Notes Line
	7100 4600 7550 4600
Wire Notes Line
	7050 4650 7600 4650
Wire Notes Line
	7600 4650 7550 5200
Wire Notes Line
	7550 5200 7500 5150
Wire Notes Line
	7500 5150 7550 4650
Wire Notes Line
	7100 4650 7050 5150
Wire Notes Line
	7050 5150 7000 5200
Wire Notes Line
	7000 5200 7050 4650
Wire Notes Line
	7050 5150 7500 5150
Wire Notes Line
	7000 5200 7550 5200
Wire Notes Line
	7650 5150 7650 5200
Wire Notes Line
	7650 5200 7700 5200
Wire Notes Line
	7700 5200 7700 5150
Wire Notes Line
	7700 5150 7650 5150
Wire Notes Line
	7650 5250 7650 5350
Wire Notes Line
	7650 5350 7700 5300
Wire Notes Line
	7700 5300 7700 5250
Wire Notes Line
	7700 5250 7650 5250
Wire Notes Line
	7100 5250 7500 5250
Wire Notes Line
	7500 5250 7300 5350
Wire Notes Line
	7300 5350 7100 5250
Wire Wire Line
	8300 4300 8400 4300
Connection ~ 8300 2900
Wire Wire Line
	8400 4200 8300 4200
Connection ~ 8300 4200
Wire Wire Line
	8400 3900 8300 3900
Connection ~ 8300 3900
Wire Wire Line
	8400 3700 8300 3700
Connection ~ 8300 3700
Wire Wire Line
	8400 3600 8300 3600
Connection ~ 8300 3600
Wire Wire Line
	8400 3500 8300 3500
Connection ~ 8300 3500
Wire Wire Line
	8400 3400 8300 3400
Connection ~ 8300 3400
Wire Wire Line
	8400 3800 8300 3800
Connection ~ 8300 3800
Text Notes 7300 2050 0    60   ~ 0
typ. 60mA, 1.1V \n(between F1 and F3)
$EndSCHEMATC
