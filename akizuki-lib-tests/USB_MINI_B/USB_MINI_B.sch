EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L USB_MINI_B J1
U 1 1 56890841
P 1300 1300
F 0 "J1" H 1000 1600 60  0000 C CNN
F 1 "USB_MINI_B" H 1200 1000 60  0000 C CNN
F 2 "fs495-akizuki-fp:MUSB-5BF01AS" H 1350 1350 60  0001 C CNN
F 3 "" H 1350 1350 60  0000 C CNN
	1    1300 1300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
