EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:MAX662-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MAX662 U1
U 1 1 568883CC
P 1300 1100
F 0 "U1" H 1300 1400 60  0000 C CNN
F 1 "MAX662" H 1300 800 60  0000 C CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 1300 1100 60  0000 C CNN
F 3 "" H 1300 1100 60  0000 C CNN
	1    1300 1100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
