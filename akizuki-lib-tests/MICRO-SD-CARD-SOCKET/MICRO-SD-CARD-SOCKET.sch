EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MICRO-SD-CARD-SOCKET J1
U 1 1 568887DC
P 1600 1450
F 0 "J1" H 1150 1950 60  0000 C CNN
F 1 "MICRO-SD-CARD-SOCKET" H 1350 850 60  0000 C CNN
F 2 "fs495-akizuki-fp:DM3AT-SF-PEJM5" H 1900 1450 60  0001 C CNN
F 3 "" H 1900 1450 60  0000 C CNN
	1    1600 1450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
