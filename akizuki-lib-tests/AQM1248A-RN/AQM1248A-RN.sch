EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:AQM1248A-RN-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AQM1248A-RN LCD1
U 1 1 56881079
P 1600 1250
F 0 "LCD1" H 1500 1650 60  0000 C CNN
F 1 "AQM1248A-RN" H 1600 850 60  0000 C CNN
F 2 "fs495-akizuki-fp:AQM1248A-RN" H 1600 1050 60  0000 C CNN
F 3 "" H 1600 1050 60  0000 C CNN
	1    1600 1250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
