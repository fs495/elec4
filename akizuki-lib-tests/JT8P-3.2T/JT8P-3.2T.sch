EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:JT8P-3.2T-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L JT8P-3.2T AJ1
U 1 1 568881FE
P 1300 1250
F 0 "AJ1" H 1000 1600 60  0000 C CNN
F 1 "JT8P-3.2T" H 1650 950 60  0000 C CNN
F 2 "fs495-akizuki-fp:JT8P-3.2T" H 1300 1100 60  0000 C CNN
F 3 "JT8P-3.2T-B10K-1-16Y" H 1350 700 60  0001 C CNN
	1    1300 1250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
