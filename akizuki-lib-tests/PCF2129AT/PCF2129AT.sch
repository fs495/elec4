EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PCF2129AT U1
U 1 1 56888F3D
P 1800 1200
F 0 "U1" H 1800 1250 60  0000 C CNN
F 1 "PCF2129AT" H 1800 150 60  0000 C CNN
F 2 "Housings_SOIC:SOIC-20_7.5x12.8mm_Pitch1.27mm" H 1800 1200 60  0001 C CNN
F 3 "" H 1800 1200 60  0000 C CNN
	1    1800 1200
	1    0    0    -1  
$EndComp
NoConn ~ 1150 1300
NoConn ~ 1150 1500
NoConn ~ 1150 1600
NoConn ~ 1150 1800
NoConn ~ 1150 2100
NoConn ~ 2450 2100
NoConn ~ 2450 2000
NoConn ~ 2450 1900
NoConn ~ 2450 1800
NoConn ~ 2450 1600
NoConn ~ 2450 1500
NoConn ~ 2450 1300
$EndSCHEMATC
