EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:fs495-akizuki-cl
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L medjed U1
U 1 1 59944A71
P 1450 1900
F 0 "U1" H 1650 2200 50  0000 C CNN
F 1 "medjed" H 1200 1550 50  0000 C CNN
F 2 "" V 1450 1900 50  0001 C CNN
F 3 "" V 1450 1900 50  0001 C CNN
	1    1450 1900
	1    0    0    -1  
$EndComp
$Comp
L medjed U2
U 1 1 59944AC7
P 2250 1900
F 0 "U2" H 2450 2200 50  0000 C CNN
F 1 "medjed" H 2520 1520 50  0000 C CNN
F 2 "" V 2250 1900 50  0001 C CNN
F 3 "" V 2250 1900 50  0001 C CNN
	1    2250 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 1400 1750 1400
Wire Wire Line
	1750 1400 1750 2300
Wire Wire Line
	1750 2300 2150 2500
Connection ~ 1450 1400
Wire Wire Line
	1550 2500 1950 2300
Wire Wire Line
	1950 2300 1950 1400
Wire Wire Line
	1950 1400 2250 1400
Connection ~ 2250 1400
Text GLabel 1350 2500 3    60   Input ~ 0
~S
Text GLabel 2350 2500 3    60   Input ~ 0
~R
Text GLabel 1450 1300 1    60   Input ~ 0
Q
Text GLabel 2250 1300 1    60   Input ~ 0
~Q
$EndSCHEMATC
