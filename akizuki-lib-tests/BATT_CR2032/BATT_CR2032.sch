EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:BATT_CR2032-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L BATT_CR2032 B1
U 1 1 5688178B
P 1700 1850
F 0 "B1" H 1700 2050 60  0000 C CNN
F 1 "BATT_CR2032" H 1750 1650 60  0000 C CNN
F 2 "fs495-akizuki-fp:CH25-2032LF" H 1650 2050 60  0000 C CNN
F 3 "" H 1650 2050 60  0000 C CNN
	1    1700 1850
	1    0    0    -1  
$EndComp
NoConn ~ 2050 1850
NoConn ~ 1350 1850
NoConn ~ 1050 1450
$EndSCHEMATC
