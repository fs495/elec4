#!/usr/bin/env ruby
# coding: utf-8

# ファミリごとのパラメータ(mm単位)
# - pin_pitch: ピン間のピッチ
# - body_se_len: ICボディ(モールド部分)の短辺長
# - l2l_se_len: リード先端から対向リードの先端までの長さ
# - lead_width: リード幅
# - lead_len: リード長
# - pad_width: パッド幅
# - pad_len: パッド長
#
# フットプリントごとのパラメータ
# - pins: ピン数
# - body_le_len: ICボディの長辺長
#
# リードは短辺l2l_se_len、長辺body_le_lenの矩形領域に内接する
# リードの中心がパッドの中心

require 'pp'

#----------------------------------------------------------------------

FAMILY_LIST = [
    # SO-32 (11.3mm body, 1.27mm pitch)
    # http://akizukidenshi.com/download/ds/st/M68AF127B.pdf
    {
	:name => 'SO-{pin}_{se_len}x{le_len}mm_P{pitch}mm',
	:pin_pitch => 1.27,
	:body_se_len => 0.445 * 25.4,
	:l2l_se_len => 0.556 * 25.4,
	:lead_width => 0.017 * 25.4,
	:lead_len => 0.031 * 25.4,
	:pad_width => 0.026 * 25.4,
	:pad_len => 0.070 * 25.4,
	:list => [
	    {
		:pins => 32,
		:body_le_len => 0.8 * 25.4,
	    }
	]
    },

    # Narrow SOIC (3.9mm body, 1.27mm pitch)
    # https://en.wikipedia.org/wiki/Small_Outline_Integrated_Circuit
    {
	:name => 'SOIC-{pin}_{se_len}x{le_len}mm_P{pitch}mm',
	:pin_pitch => 1.27,
	:body_se_len => 3.9,
	:l2l_se_len => 6.0,
	:lead_width => 0.42,
	:lead_len => 1.05,
	:pad_width => 0.6,
	:pad_len => 1.5,
	:list => [
	    {
		:pins => 8,
		:body_le_len => 4.9,
	    },
	    {
		:pins => 14,
		:body_le_len => 8.7,
	    },
	    {
		:pins => 16,
		:body_le_len => 9.9,
	    },
	]
    },
    
    # Wide SOIC (5.3mm body, 1.27mm pitch)
    {
	:name => 'SOIC-{pin}_{se_len}x{le_len}mm_P{pitch}mm',
	:pin_pitch => 1.27,
	:body_se_len => 5.3,
	:l2l_se_len => 7.9,
	:lead_width => 0.42,
	:lead_len => 1.05,
	:pad_width => 0.6,
	:pad_len => 1.5,
	:list => [
	    {
		:pins => 8,
		:body_le_len => 5.3,
	    },
	    {
		:pins => 20,
		:body_le_len => 12.8,
	    }
	]
    },

    # Wide SOIC (7.5mm body, 1.27mm pitch)
    # SOIC-16: https://en.wikipedia.org/wiki/Small_Outline_Integrated_Circuit
    # Others: KiCad Standard footprint lib
    {
	:name => 'SOIC-{pin}_{se_len}x{le_len}mm_P{pitch}mm',
	:pin_pitch => 1.27,
	:body_se_len => 7.5,
	:l2l_se_len => 10.3,
	:lead_width => 0.42,
	:lead_len => 1.05,
	:pad_width => 0.6,
	:pad_len => 1.5,
	:list => [
	    {
		:pins => 16,
		:body_le_len => 10.3,
	    },
	    {
		:pins => 18,
		:body_le_len => 11.6,
	    },
	    {
		:pins => 20,
		:body_le_len => 12.8,
	    },
	    {
		:pins => 24,
		:body_le_len => 15.4,
	    },
	    {
		:pins => 28,
		:body_le_len => 17.9,
	    },
	]
    },

    # SSOP (5.3mm, 0.65mm pitch)
    # http://www.analog.com/jp/design-center/packaging-quality-symbols-footprints/package-index/sop-small-outline-package/ssop-shrink-so.html
    {
	:name => 'SSOP-{pin}_{se_len}x{le_len}mm_P{pitch}mm',
	:pin_pitch => 0.65,
	:body_se_len => 5.3,
	:l2l_se_len => 7.8,
	:lead_width => 0.25,
	:lead_len => 0.75,
	:pad_width => 0.30, # not shown
	:pad_len => 1.00, # not shown
	:list => [
	    {
		:pins => 16,
		:body_le_len => 6.20,
	    },
	    {
		:pins => 20,
		:body_le_len => 7.20,
	    },
	    {
		:pins => 24,
		:body_le_len => 8.20,
	    },
	    {
		:pins => 28,
		:body_le_len => 10.20,
	    },
	]
    },
	
    # QSOP (3.8mm, 0.64mm pitch)
    # http://www.analog.com/jp/design-center/packaging-quality-symbols-footprints/package-index/sop-small-outline-package/qsop-25mil-pitch.html
    {
	:name => 'QSOP-{pin}_{se_len}x{le_len}mm_P{pitch}mm',
	:pin_pitch => 0.025 * 25.4,
	:body_se_len => 0.154 * 25.4,
	:l2l_se_len => 0.236 * 25.4,
	:lead_width => 0.010 * 25.4,
	:lead_len => 0.041 * 25.4,
	:pad_width => 0.015 * 25.4, # not shown
	:pad_len => 0.060 * 25.4, # not shown
	:list => [
	    {
		:pins => 16,
		:body_le_len => 0.193 * 25.4
	    },
	    {
		:pins => 20,
		:body_le_len => 0.341 * 25.4,
	    },
	    {
		:pins => 24,
		:body_le_len => 0.341 * 25.4,
	    },
	    {
		:pins => 28,
		:body_le_len => 0.390 * 25.4,
	    },
	]
    },
	
]

#----------------------------------------------------------------------

class FPgenerator
    def method_missing(name, *args)
	if @member.has_key?(name)
	    return @member[name]
	elsif @common.has_key?(name)
	    return @common[name]
	else
	    super
	end
    end

    def fmt(value, format = '%.3f')
	return format % value
    end
    
    def output(family)
	@common = family.dup
	@common.delete(:list)
	family[:list].each do |member|
	    @member = member
	    do_output()
	end
    end

    def do_output()
	fp_name = name.gsub('{pin}', pins.to_s)
		     .gsub('{se_len}', fmt(body_se_len, '%.1f'))
		     .gsub('{le_len}', fmt(body_le_len, '%.1f'))
		     .gsub('{pitch}', fmt(pin_pitch, '%.2f'))
		     .gsub(' ', '_')
	out = open("#{fp_name}.kicad_mod", "w")

	# プロローグ
	out.puts "(module #{fp_name} (layer F.Cu) (tedit #{fp_name.hash.abs})"
	out.puts "  (tags \"SOP #{pin_pitch}\")"
	out.puts "  (attr smd)"

	# 文字情報
	out.puts <<"EOL"
  (fp_text reference REF** (at 0 #{-body_le_len/2-1}) (layer F.SilkS)
    (effects (font (size 1 1) (thickness 0.15)))
  )
  (fp_text value #{fp_name} (at 0 #{body_le_len/2+1}) (layer F.Fab)
    (effects (font (size 1 1) (thickness 0.15)))
  )
  (fp_text user %R (at 0 0) (layer F.Fab)
    (effects (font (size 1 1) (thickness 0.15)))
  )
EOL

	# ICボディ
	bx, by = body_se_len/2, body_le_len/2
	fbx, fbx1 = (fmt bx), (fmt bx - 1)
	fby, fby1 = (fmt by), (fmt by - 1)
	out.puts <<"EOL"
  (fp_line (start -#{fbx1} -#{fby}) (end  #{fbx} -#{fby}) (layer F.Fab) (width 0.15))
  (fp_line (start  #{fbx} -#{fby}) (end  #{fbx}  #{fby}) (layer F.Fab) (width 0.15))
  (fp_line (start  #{fbx}  #{fby}) (end -#{fbx}  #{fby}) (layer F.Fab) (width 0.15))
  (fp_line (start -#{fbx}  #{fby}) (end -#{fbx} -#{fby1}) (layer F.Fab) (width 0.15))
  (fp_line (start -#{fbx} -#{fby1}) (end -#{fbx1} -#{fby}) (layer F.Fab) (width 0.15))
EOL

	# リード
	padx = l2l_se_len/2 - lead_len/2 # パッドの中央X座標
	l1x, l2x = padx + lead_len/2, padx - lead_len/2
	fl1x, fl2x = (fmt l1x), (fmt l2x)
	out.puts <<"EOL"
  (fp_line (start -#{fl1x}  #{fby}) (end -#{fl1x} -#{fby}) (layer F.Fab) (width 0.05))
  (fp_line (start -#{fl2x}  #{fby}) (end -#{fl2x} -#{fby}) (layer F.Fab) (width 0.05))
  (fp_line (start  #{fl1x}  #{fby}) (end  #{fl1x} -#{fby}) (layer F.Fab) (width 0.05))
  (fp_line (start  #{fl2x}  #{fby}) (end  #{fl2x} -#{fby}) (layer F.Fab) (width 0.05))
EOL

	# フットプリント(ICボディ+リード+パッド + シルク)
	fpx, fpy = padx + pad_len/2, body_le_len/2 + 0.15 * 1.5
	ffpx, ffpy = (fmt fpx), (fmt fpy)
	out.puts <<"EOL"
  (fp_line (start -#{ffpx} -#{ffpy}) (end  #{ffpx} -#{ffpy}) (layer F.CrtYd) (width 0.05))
  (fp_line (start  #{ffpx} -#{ffpy}) (end  #{ffpx}  #{ffpy}) (layer F.CrtYd) (width 0.05))
  (fp_line (start  #{ffpx}  #{ffpy}) (end -#{ffpx}  #{ffpy}) (layer F.CrtYd) (width 0.05))
  (fp_line (start -#{ffpx}  #{ffpy}) (end -#{ffpx} -#{ffpy}) (layer F.CrtYd) (width 0.05))
EOL
	# シルク
	six, siy = body_se_len/2 + 0.15, body_le_len/2 + 0.15
	fsix, fsiy = (fmt six), (fmt siy)
	out.puts "  (fp_line (start -#{fsix}  #{fsiy}) (end  #{fsix}  #{fsiy}) (layer F.SilkS) (width 0.15))"
	out.puts "  (fp_line (start -#{fsix}  #{fsiy}) (end -#{fsix}  #{fby}) (layer F.SilkS) (width 0.15))"
	out.puts "  (fp_line (start  #{fsix}  #{fsiy}) (end  #{fsix}  #{fby}) (layer F.SilkS) (width 0.15))"

	out.puts "  (fp_line (start -#{fsix} -#{fsiy}) (end  #{fsix} -#{fsiy}) (layer F.SilkS) (width 0.15))"
	out.puts "  (fp_line (start -#{fsix} -#{fsiy}) (end -#{fsix} -#{fby}) (layer F.SilkS) (width 0.15))"
	out.puts "  (fp_line (start  #{fsix} -#{fsiy}) (end  #{fsix} -#{fby}) (layer F.SilkS) (width 0.15))"
	out.puts "  (fp_line (start -#{fsix} -#{fby}) (end -#{ffpx} -#{fby}) (layer F.SilkS) (width 0.15))"

	# ハンダ領域(パッド+マージン)
	(pins / 2).times do |n|
	    lpin, rpin = n + 1, pins - n
	    flpin, frpin = "%2s" % lpin, "%2s" % rpin

	    # パッド/リードの中心座標
	    px, py = padx, (n - (pins - 2) / 4.0) * pin_pitch
	    fpx, fpy = (fmt px), (fmt py, '% .3f')

	    # パッド
	    out.puts "  (pad #{flpin} smd rect (at -#{fpx} #{fpy}) (size #{fmt pad_len} #{fmt pad_width}) (layers F.Cu F.Paste F.Mask))"
	    out.puts "  (pad #{frpin} smd rect (at  #{fpx} #{fpy}) (size #{fmt pad_len} #{fmt pad_width}) (layers F.Cu F.Paste F.Mask))"
	end
	out.puts ")"

	out.close
    end
end

if $0 == __FILE__
    gen = FPgenerator.new
    FAMILY_LIST.each do |family|
	gen.output(family)
    end
end
