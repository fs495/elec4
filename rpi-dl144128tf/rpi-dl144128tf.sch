EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:rpi-dl144128tf-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RPi_GPIO_Rev2 P1
U 1 1 56927C44
P 2050 1350
F 0 "P1" H 1900 1600 60  0000 C CNN
F 1 "RPi_GPIO_Rev2" H 1900 1500 60  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x13" H 1950 1450 60  0001 C CNN
F 3 "" H 2050 1550 60  0000 C CNN
	1    2050 1350
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X08 P2
U 1 1 56927CA7
P 5900 1700
F 0 "P2" H 5900 2150 50  0000 C CNN
F 1 "CONN_01X08" V 6000 1700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x08" H 5900 1700 50  0001 C CNN
F 3 "" H 5900 1700 50  0000 C CNN
	1    5900 1700
	1    0    0    -1  
$EndComp
Text Label 5450 1350 0    60   ~ 0
VCC
Text Label 5450 1450 0    60   ~ 0
GND
Text Label 5450 1550 0    60   ~ 0
CS
Text Label 5450 1650 0    60   ~ 0
RESET
Text Label 5450 1750 0    60   ~ 0
A0
Text Label 5450 1850 0    60   ~ 0
MIMO
Text Label 5450 1950 0    60   ~ 0
SCLK
Text Label 5450 2050 0    60   ~ 0
LED
Wire Wire Line
	2050 1350 5700 1350
Wire Wire Line
	2050 1450 2150 1450
Wire Wire Line
	2150 1450 2150 1350
Connection ~ 2150 1350
Wire Wire Line
	2250 1450 5700 1450
Wire Wire Line
	2250 1450 2250 1950
Wire Wire Line
	2250 1850 2050 1850
Wire Wire Line
	2250 1950 2050 1950
Connection ~ 2250 1850
$Comp
L TEST_1P W8
U 1 1 56927E37
P 2300 3650
F 0 "W8" V 2350 3700 50  0000 C CNN
F 1 "CE1" V 2300 3900 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 2500 3650 50  0001 C CNN
F 3 "" H 2500 3650 50  0000 C CNN
	1    2300 3650
	0    1    1    0   
$EndComp
$Comp
L TEST_1P W9
U 1 1 56927EF1
P 2300 3750
F 0 "W9" V 2350 3800 50  0000 C CNN
F 1 "CE0" V 2300 4000 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 2500 3750 50  0001 C CNN
F 3 "" H 2500 3750 50  0000 C CNN
	1    2300 3750
	0    1    1    0   
$EndComp
$Comp
L TEST_1P W12
U 1 1 56927F2D
P 3100 3700
F 0 "W12" V 3150 3750 50  0000 C CNN
F 1 "CE" V 3100 3900 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01" H 3300 3700 50  0001 C CNN
F 3 "" H 3300 3700 50  0000 C CNN
	1    3100 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3500 1550 5700 1550
Wire Wire Line
	3500 1550 3500 3700
Wire Wire Line
	2300 3650 2050 3650
Wire Wire Line
	2050 3750 2300 3750
Wire Wire Line
	3300 1650 5700 1650
Wire Wire Line
	3400 1750 5700 1750
Wire Wire Line
	3600 1850 5700 1850
Wire Wire Line
	3600 1850 3600 3950
Wire Wire Line
	3600 3950 2050 3950
Wire Wire Line
	3700 1950 5700 1950
Wire Wire Line
	2050 4050 3700 4050
Wire Wire Line
	3700 4050 3700 1950
Wire Wire Line
	3850 1350 3850 2050
Wire Wire Line
	3850 2050 5700 2050
Connection ~ 3850 1350
Wire Wire Line
	3500 3700 3100 3700
$Comp
L TEST_1P W7
U 1 1 5692863E
P 2300 3550
F 0 "W7" V 2350 3600 50  0000 C CNN
F 1 "IO27" V 2300 3800 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 2500 3550 50  0001 C CNN
F 3 "" H 2500 3550 50  0000 C CNN
	1    2300 3550
	0    1    1    0   
$EndComp
$Comp
L TEST_1P W6
U 1 1 56928678
P 2300 3450
F 0 "W6" V 2350 3500 50  0000 C CNN
F 1 "IO25" V 2300 3700 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 2500 3450 50  0001 C CNN
F 3 "" H 2500 3450 50  0000 C CNN
	1    2300 3450
	0    1    1    0   
$EndComp
$Comp
L TEST_1P W4
U 1 1 569286A2
P 2300 3250
F 0 "W4" V 2350 3300 50  0000 C CNN
F 1 "IO23" V 2300 3500 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 2500 3250 50  0001 C CNN
F 3 "" H 2500 3250 50  0000 C CNN
	1    2300 3250
	0    1    1    0   
$EndComp
$Comp
L TEST_1P W5
U 1 1 569286EC
P 2300 3350
F 0 "W5" V 2350 3400 50  0000 C CNN
F 1 "IO24" V 2300 3600 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 2500 3350 50  0001 C CNN
F 3 "" H 2500 3350 50  0000 C CNN
	1    2300 3350
	0    1    1    0   
$EndComp
$Comp
L TEST_1P W3
U 1 1 56928714
P 2300 3150
F 0 "W3" V 2350 3200 50  0000 C CNN
F 1 "IO22" V 2300 3400 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 2500 3150 50  0001 C CNN
F 3 "" H 2500 3150 50  0000 C CNN
	1    2300 3150
	0    1    1    0   
$EndComp
$Comp
L TEST_1P W2
U 1 1 56928A36
P 2300 3050
F 0 "W2" V 2350 3100 50  0000 C CNN
F 1 "IO18" V 2300 3300 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 2500 3050 50  0001 C CNN
F 3 "" H 2500 3050 50  0000 C CNN
	1    2300 3050
	0    1    1    0   
$EndComp
$Comp
L TEST_1P W1
U 1 1 56928A6E
P 2300 2950
F 0 "W1" V 2350 3000 50  0000 C CNN
F 1 "IO17" V 2300 3200 50  0000 C CNN
F 2 "Wire_Pads:SolderWirePad_single_0-8mmDrill" H 2500 2950 50  0001 C CNN
F 3 "" H 2500 2950 50  0000 C CNN
	1    2300 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	2050 2950 2300 2950
Wire Wire Line
	2050 3050 2300 3050
Wire Wire Line
	2050 3150 2300 3150
Wire Wire Line
	2050 3250 2300 3250
Wire Wire Line
	2050 3350 2300 3350
Wire Wire Line
	2050 3450 2300 3450
Wire Wire Line
	2050 3550 2300 3550
NoConn ~ 2050 3850
NoConn ~ 2050 2850
NoConn ~ 2050 2750
NoConn ~ 2050 2650
NoConn ~ 2050 2550
NoConn ~ 2050 2450
NoConn ~ 2050 2250
NoConn ~ 2050 2150
NoConn ~ 2050 2050
NoConn ~ 2050 1550
NoConn ~ 2050 1650
$Comp
L TEST_1P W11
U 1 1 56928E2A
P 3100 3450
F 0 "W11" V 3150 3500 50  0000 C CNN
F 1 "A0" V 3100 3650 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01" H 3300 3450 50  0001 C CNN
F 3 "" H 3300 3450 50  0000 C CNN
	1    3100 3450
	0    -1   -1   0   
$EndComp
$Comp
L TEST_1P W10
U 1 1 56928F48
P 3100 3300
F 0 "W10" V 3150 3350 50  0000 C CNN
F 1 "RESET" V 3100 3550 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x01" H 3300 3300 50  0001 C CNN
F 3 "" H 3300 3300 50  0000 C CNN
	1    3100 3300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3400 1750 3400 3450
Wire Wire Line
	3400 3450 3100 3450
Wire Wire Line
	3300 1650 3300 3300
Wire Wire Line
	3300 3300 3100 3300
$EndSCHEMATC
