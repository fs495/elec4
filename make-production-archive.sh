#!/bin/sh

set -e

PCBNAME=$1

	rm -rf work
	mkdir work
	cp -p ${PCBNAME}.drl work/${PCBNAME}.TXT
	cp -p ${PCBNAME}-F.Cu.gbr work/${PCBNAME}.GTL
	cp -p ${PCBNAME}-B.Cu.gbr work/${PCBNAME}.GBL
	cp -p ${PCBNAME}-F.Mask.gbr work/${PCBNAME}.GTS
	cp -p ${PCBNAME}-B.Mask.gbr work/${PCBNAME}.GBS
	cp -p ${PCBNAME}-F.SilkS.gbr work/${PCBNAME}.GTO
	cp -p ${PCBNAME}-B.SilkS.gbr work/${PCBNAME}.GBO
	cp -p ${PCBNAME}-Edge.Cuts.gbr work/${PCBNAME}.GML
	rm -f ${PCBNAME}_pcb.zip
	(cd work && zip ../${PCBNAME}_pcb.zip *)
	rm -r work
