EESchema Schematic File Version 2
LIBS:fs495-akizuki-cl
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR01
U 1 1 568E7996
P 2650 3500
F 0 "#PWR01" H 2650 3250 50  0001 C CNN
F 1 "GND" H 2650 3350 50  0000 C CNN
F 2 "" H 2650 3500 50  0000 C CNN
F 3 "" H 2650 3500 50  0000 C CNN
	1    2650 3500
	1    0    0    -1  
$EndComp
$Comp
L MAX232 U1
U 1 1 56F7584F
P 3700 2200
F 0 "U1" H 3250 3050 50  0000 L CNN
F 1 "MAX232" H 3900 3050 50  0000 L CNN
F 2 "Housings_DIP:DIP-16_W7.62mm" H 3700 1350 50  0000 C CNN
F 3 "" H 3700 2200 50  0000 C CNN
	1    3700 2200
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 56F75C23
P 2900 1700
F 0 "C1" H 2925 1800 50  0000 L CNN
F 1 "0.1u" H 2925 1600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 2938 1550 50  0001 C CNN
F 3 "" H 2900 1700 50  0000 C CNN
	1    2900 1700
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 56F75D34
P 2900 2200
F 0 "C2" H 2925 2300 50  0000 L CNN
F 1 "0.1u" H 2925 2100 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 2938 2050 50  0001 C CNN
F 3 "" H 2900 2200 50  0000 C CNN
	1    2900 2200
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 56F75D9D
P 4500 1700
F 0 "C3" H 4525 1800 50  0000 L CNN
F 1 "0.1u" H 4525 1600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 4538 1550 50  0001 C CNN
F 3 "" H 4500 1700 50  0000 C CNN
	1    4500 1700
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 56F75E4F
P 4500 2250
F 0 "C4" H 4525 2350 50  0000 L CNN
F 1 "0.1u" H 4525 2150 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 4538 2100 50  0001 C CNN
F 3 "" H 4500 2250 50  0000 C CNN
	1    4500 2250
	1    0    0    -1  
$EndComp
$Comp
L DB9 J1
U 1 1 56F76899
P 5800 3000
F 0 "J1" H 5800 3550 50  0000 C CNN
F 1 "DB9" H 5800 2450 50  0000 C CNN
F 2 "Connect:DB9MC" H 5800 2400 50  0000 C CNN
F 3 "" H 5800 3000 50  0000 C CNN
	1    5800 3000
	1    0    0    -1  
$EndComp
Text Label 5050 3400 0    60   ~ 0
DTE_DCD
NoConn ~ 5350 3400
Text Label 5050 3300 0    60   ~ 0
DTE_DSR
Text Label 5050 3200 0    60   ~ 0
DTE_RxD
Text Label 5050 3100 0    60   ~ 0
DTE_RTS
Text Label 5050 3000 0    60   ~ 0
DTE_TxD
Text Label 5050 2900 0    60   ~ 0
DTE_CTS
Text Label 5050 2800 0    60   ~ 0
DTE_DTR
Text Label 5050 2700 0    60   ~ 0
DTE_RI
Text Label 5050 2600 0    60   ~ 0
DTE_GND
Text Notes 3250 3500 0    60   ~ 0
* This module is designed as DCE.
NoConn ~ 5350 2700
NoConn ~ 5350 2800
NoConn ~ 5350 3300
$Comp
L CONN_01X06 P1
U 1 1 56F78D36
P 750 2950
F 0 "P1" H 750 3300 50  0000 C CNN
F 1 "CONN_01X06" V 850 2950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H -200 2500 50  0000 C CNN
F 3 "" H 750 2950 50  0000 C CNN
	1    750  2950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X06 P3
U 1 1 56F78E10
P 1850 2950
F 0 "P3" H 1850 3300 50  0000 C CNN
F 1 "CONN_01X06" V 1950 2950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 2000 2500 50  0000 C CNN
F 3 "" H 1850 2950 50  0000 C CNN
	1    1850 2950
	-1   0    0    -1  
$EndComp
Text Label 2100 2700 0    60   ~ 0
DCE_VCC
Text Label 2100 2800 0    60   ~ 0
DCE_RxD
Text Label 2100 2900 0    60   ~ 0
DCE_CTS
Text Label 2100 3000 0    60   ~ 0
DCE_TxD
Text Label 2100 3100 0    60   ~ 0
DCE_RTS
$Comp
L GND #PWR02
U 1 1 56F7A8B9
P 4900 2700
F 0 "#PWR02" H 4900 2450 50  0001 C CNN
F 1 "GND" H 4900 2550 50  0000 C CNN
F 2 "" H 4900 2700 50  0000 C CNN
F 3 "" H 4900 2700 50  0000 C CNN
	1    4900 2700
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG03
U 1 1 56F7AC4E
P 2900 1200
F 0 "#FLG03" H 2900 1295 50  0001 C CNN
F 1 "PWR_FLAG" H 2900 1380 50  0000 C CNN
F 2 "" H 2900 1200 50  0000 C CNN
F 3 "" H 2900 1200 50  0000 C CNN
	1    2900 1200
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG04
U 1 1 56F7ADA3
P 2700 3400
F 0 "#FLG04" H 2700 3495 50  0001 C CNN
F 1 "PWR_FLAG" H 2700 3580 50  0000 C CNN
F 2 "" H 2700 3400 50  0000 C CNN
F 3 "" H 2700 3400 50  0000 C CNN
	1    2700 3400
	0    1    1    0   
$EndComp
$Comp
L VCC #PWR05
U 1 1 56F7B431
P 2650 1150
F 0 "#PWR05" H 2650 1000 50  0001 C CNN
F 1 "VCC" H 2650 1300 50  0000 C CNN
F 2 "" H 2650 1150 50  0000 C CNN
F 3 "" H 2650 1150 50  0000 C CNN
	1    2650 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1500 2900 1500
Wire Wire Line
	2900 1500 2900 1550
Wire Wire Line
	2900 1850 2900 1900
Wire Wire Line
	2900 1900 3100 1900
Wire Wire Line
	3100 2000 2900 2000
Wire Wire Line
	2900 2000 2900 2050
Wire Wire Line
	2900 2350 2900 2400
Wire Wire Line
	2900 2400 3100 2400
Wire Wire Line
	4300 1500 4500 1500
Wire Wire Line
	4500 1250 4500 1550
Wire Wire Line
	4300 1900 4500 1900
Wire Wire Line
	4500 1900 4500 1850
Wire Wire Line
	4300 2100 4900 2100
Wire Wire Line
	4300 2400 4500 2400
Wire Wire Line
	950  2800 3100 2800
Connection ~ 2050 2800
Wire Wire Line
	950  2900 3100 2900
Connection ~ 2050 2900
Wire Wire Line
	950  3000 2800 3000
Connection ~ 2050 3000
Wire Wire Line
	950  3100 2900 3100
Connection ~ 2050 3100
Wire Wire Line
	950  3200 2650 3200
Wire Wire Line
	2650 3200 2650 3500
Connection ~ 2050 3200
Wire Wire Line
	5350 3000 4600 3000
Wire Wire Line
	4600 3000 4600 2800
Wire Wire Line
	4500 2900 4500 3100
Wire Wire Line
	4500 3100 5350 3100
Wire Wire Line
	5350 3200 4400 3200
Wire Wire Line
	4400 3200 4400 2600
Wire Wire Line
	5350 2600 4900 2600
Wire Wire Line
	2650 2700 950  2700
Wire Wire Line
	2650 1150 2650 2700
Wire Wire Line
	2650 1250 4900 1250
Connection ~ 2650 1250
Connection ~ 4500 1500
Wire Wire Line
	4600 2800 4300 2800
Wire Wire Line
	4400 2600 4300 2600
Wire Wire Line
	4500 2900 4300 2900
Wire Wire Line
	4300 2700 4700 2700
Wire Wire Line
	4700 2700 4700 2900
Wire Wire Line
	4700 2900 5350 2900
Wire Wire Line
	2900 2800 2900 2800
Wire Wire Line
	2800 2900 2800 2900
Wire Wire Line
	2800 2600 3100 2600
Wire Wire Line
	3100 2700 2900 2700
Wire Wire Line
	2900 2700 2900 3100
Text Label 2100 3200 0    60   ~ 0
DCE_GND
Wire Wire Line
	2800 3000 2800 2600
Connection ~ 2050 2700
$Comp
L GND #PWR06
U 1 1 56F7D8CE
P 4900 2200
F 0 "#PWR06" H 4900 1950 50  0001 C CNN
F 1 "GND" H 4900 2050 50  0000 C CNN
F 2 "" H 4900 2200 50  0000 C CNN
F 3 "" H 4900 2200 50  0000 C CNN
	1    4900 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1850 4900 2200
Connection ~ 4500 2100
Wire Wire Line
	2700 3400 2650 3400
Connection ~ 2650 3400
Wire Wire Line
	2900 1250 2900 1200
Connection ~ 2900 1250
Wire Wire Line
	4900 2600 4900 2700
$Comp
L C C5
U 1 1 56F7EE06
P 4900 1700
F 0 "C5" H 4925 1800 50  0000 L CNN
F 1 "0.1u" H 4925 1600 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 4938 1550 50  0001 C CNN
F 3 "" H 4900 1700 50  0000 C CNN
	1    4900 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1250 4900 1550
Connection ~ 4500 1250
Connection ~ 4900 2100
$Comp
L CONN_01X06 P2
U 1 1 56F817AB
P 1250 2950
F 0 "P2" H 1250 3300 50  0000 C CNN
F 1 "CONN_01X06" V 1350 2950 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06" H 800 2500 50  0000 C CNN
F 3 "" H 1250 2950 50  0000 C CNN
	1    1250 2950
	-1   0    0    -1  
$EndComp
Connection ~ 1450 2700
Connection ~ 1450 2800
Connection ~ 1450 2900
Connection ~ 1450 3000
Connection ~ 1450 3100
Connection ~ 1450 3200
$EndSCHEMATC
